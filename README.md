# cinetvext

Ce dépôt contient les fichiers CSV qui représentent des tables supplémentaires dans la base de données CineTV de la Cinémathèque québécoise.

Ces données n'existent pas dans la version originale de CineTV.
